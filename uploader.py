import os
import logging
from os import listdir
from os.path import isfile, join
from requests import get
from telethon import TelegramClient, events, sync


api_id = os.environ["API_ID"]
api_hash = os.environ["API_HASH"]

client = TelegramClient("session", api_id, api_hash)
client.start()


CHANNEL_ID = os.environ["CHANNEL_ID"]

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)


build_path = "."
tempfiles = [f for f in listdir(build_path) if isfile(join(build_path, f))]

build_files = []
for file in tempfiles:
    if file.endswith(".exe"):
        build_files.append(file)
    elif file.endswith(".dmg"):
        build_files.append(file)
    elif file.endswith(".AppImage"):
        build_files.append(file)
    elif file.endswith(".deb"):
        build_files.append(file)
if not len(build_files):
    logging.fatal("No binaries found.")
build_files = sorted(build_files)


COMMIT_ID = os.environ["BUILD_SOURCEVERSION"]

# getting commit message to post on tg message
api_url = f"https://gitlab.com/api/v4/projects/RemixDev%2Fdeemix-gui/repository/commits/{COMMIT_ID}"

req = get(api_url)
req.raise_for_status()
commit_msg = req.json()["message"]

commit_url = f"https://gitlab.com/RemixDev/deemix-gui/commit/{COMMIT_ID}"

client.send_message(CHANNEL_ID, f"Commit [{COMMIT_ID[:10]}]({commit_url})\n{commit_msg}", link_preview=False)

for file in build_files:
    logging.info("Uploading " + file + "...")
    curr_build_path = join(build_path, file)
    client.send_file(CHANNEL_ID, curr_build_path)
